import UIKit

class NoteListViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var noteListTableView: UITableView!
    var notesTextField: UITextField?
    private let refreshControl = UIRefreshControl()
    var note: [Note] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupScreenStyle()
        noteListTableView.dataSource = self
        noteListTableView.delegate = self
        fetchNotes()
    }
    
    @IBAction func addNotesButtonAction(_ sender: UIBarButtonItem) {
        popUpController()
    }
    
    @objc func fetchNotes() {
        NoteService().getAllNotes { result in
            switch result {
            case .failure(let error):
                print(error.localizedDescription)
                self.refreshControl.endRefreshing()
            case .success(let notes):
                self.note = notes
                DispatchQueue.main.async {
                    self.noteListTableView.reloadData()
                    self.refreshControl.endRefreshing()
                }
            }
        }
    }
    
    func popUpController() {
        let alertController = UIAlertController(title: "New Note",
                                                message: "Please, add new note",
                                                preferredStyle: .alert)
        
        alertController.addTextField { [weak self] textFiled in
            self?.notesTextField = textFiled
        }
        
        let saveAction = UIAlertAction(title: "Save Note",
                                       style: .default,
                                       handler: { (alert: UIAlertAction) in
                                        // Create POST Request
        })
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel,
                                         handler: nil)
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion:{ })
    }
    
    func setupScreenStyle() {
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.barTintColor = UIColor.black
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        noteListTableView.refreshControl = refreshControl
        refreshControl.tintColor = .white
        refreshControl.attributedTitle = NSAttributedString(string: "Fetching Notes ...",
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        refreshControl.addTarget(self,
                                 action: #selector(fetchNotes),
                                 for: .valueChanged)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "DetailedNote") {
            if let indexPath = noteListTableView.indexPathForSelectedRow {
                let selectedRow = indexPath.row
                guard let viewController: DetailedNoteViewController = segue.destination as? DetailedNoteViewController else { return }
                viewController.noteModel = note[selectedRow]
            }
        }
    }
}

extension NoteListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension NoteListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return note.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NoteCell",
                                                       for: indexPath) as? NoteCell else { return UITableViewCell() }
        
        cell.noteIDLabel.text = String(note[indexPath.row].noteID)
        cell.noteTitleLabel.text = note[indexPath.row].noteTitle
        
        return cell
    }
}
