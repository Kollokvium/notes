import UIKit

class DetailedNoteViewController: UIViewController {

    @IBOutlet weak var noteIDlabel: UILabel!
    @IBOutlet weak var noteTextView: UITextView!
    
    var noteModel: Note!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupStyle()
        setupContent()
        setupObserving()
    }
    
    fileprivate func setupContent() {
        if noteIDlabel.text != nil && noteTextView != nil {
            noteIDlabel.text = "Note ID: \(String(noteModel.noteID))"
            noteTextView.text = noteModel.noteTitle
        }
    }
    
    fileprivate func setupStyle() {
        title = String(noteModel.noteID)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: I.images.backArrow,
                                                           style: .done,
                                                           target: self,
                                                           action: #selector(popVC))
        navigationItem.leftBarButtonItem?.tintColor = .white
        navigationController?.navigationBar.prefersLargeTitles = false
        noteTextView.isEditable = true
    }
    
    func setupObserving() {
        noteTextView.delegate = self
        let tap = UITapGestureRecognizer(target: self,
                                         action: #selector(DetailedNoteViewController.handleTap))
        view.addGestureRecognizer(tap)
    }
    
    @objc private func popVC() {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveNoteAction(_ sender: UIBarButtonItem) {
        NoteService().saveNoteToRemote(noteModel, completion: { result in
            switch result {
            case .success(let responseModel):
                self.noteModel = responseModel
            case .failure(let error):
                print(error)
            }
        })
    }
    
    // MARK: UITapGestureRecognizer
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        view.endEditing(true)
    }
}

extension DetailedNoteViewController: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        
    }
}
