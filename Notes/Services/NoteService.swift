import Foundation

public enum Result<T, E: Error> {
    case success(T)
    case failure(E)
}

struct NoteService {
    enum Constants {
        static let baseURL = "http://private-9aad-note10.apiary-mock.com/"
        static let notesURL = "notes"
    }
    
    enum ApiServiceError: Error {
        case empty
        case decoder(error: Error)
    }
    
    func getAllNotes(completion: @escaping (_ result: Result<[Note], ApiServiceError>) -> Void) {
        guard let baseURL = URL(string: Constants.baseURL + Constants.notesURL) else { return }
        var request = URLRequest(url: baseURL)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else { return completion(.failure(.empty)) }
            do {
                let decoder = JSONDecoder()
                let noteData3 = try decoder.decode([Note].self, from: data)
                completion(.success(noteData3))
            } catch {
                completion(.failure(.decoder(error: error)))
            }
            }.resume()
    }
    
    func saveNoteToRemote(_ note: Note, completion: @escaping(_ result: Result<Note, ApiServiceError>) -> Void) {
        guard let baseURL = URL(string: Constants.baseURL + Constants.notesURL + "/" + String(note.noteID)) else { return }
        let session = URLSession.shared
        var request = URLRequest(url: baseURL)
        request.httpMethod = "PUT"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let params: [String: String] = ["title" : note.noteTitle,
                                        "id" : String(note.noteID)]
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params,
                                                          options: JSONSerialization.WritingOptions())
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                if let response = response {
                    guard let nsHTTPResponse = response as? HTTPURLResponse else { return }
                    let statusCode = nsHTTPResponse.statusCode
                    print ("status code = \(statusCode)")
                }
                if let error = error {
                    print ("\(error)")
                    completion(.failure(.decoder(error: error)))
                }
                if let data = data {
                    do {
                        let jsonResponse = try JSONSerialization.jsonObject(with: data,
                                                                            options: JSONSerialization.ReadingOptions())
                        print ("data = \(jsonResponse)")
                    } catch _ {
                        print ("dich prishla")
                    }
                }
            })
            task.resume()
        } catch _ {
            print ("Unknown Error")
        }
    }
}
