import UIKit

extension UIImage {
    static let images = Images()
    
    struct Images {
        let backArrow = UIImage(named: "backArrow")!
    }
}

public typealias I = UIImage
