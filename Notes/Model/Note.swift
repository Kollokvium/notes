import Foundation

struct Note: Codable {
    let noteID: Int
    let noteTitle: String
    
    enum CodingKeys: String, CodingKey {
        case noteID = "id"
        case noteTitle = "title"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        noteID = try values.decode(Int.self, forKey: .noteID)
        noteTitle = try values.decode(String.self, forKey: .noteTitle)
    }
}
